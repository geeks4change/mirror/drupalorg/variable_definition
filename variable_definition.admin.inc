<?php

/**
 * Variable def admin form
 */

/**
 * get-title helper
 */
function variable_definition_get_title_helper($v) {
  return $v['title'];
}

/**
 * options list helper
 */
function variable_definition_options_list_helper($v) {
  $result = array_map('variable_definition_get_title_helper', $v);
  asort($result);
  return $result;
}

/**
* Form API callback for the variable form.
*/
function variable_definition_form($form, &$form_state, $item, $op = 'edit') {
  if($op == 'add') {
    $item = (object)array(
      'name' => '',
      'type' => 'default',
      'vargroup' => 'default',
      'title' => '',
      'description' => '',
      'required' => FALSE,
      'token' => FALSE,
      'localize' => FALSE,
      'multidomain' => FALSE,
    );
  }
  $form['admin_ui_op'] = array(
      '#type' => 'hidden',
      '#value' => $op,
  );
  $form['title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $item->title,
  );
  $form['name'] = array(
      '#title' => t('Machine Name'),
      '#description' => 'Must only contain lowercase letters, numbers and underscore. Must be unique and cannot be changed later.',
      '#type' => 'machine_name',
      '#default_value' => $item->name,
      '#maxlength' => 255,
      '#machine_name' => array(
  			'source' => array('title'),
  			'exists' => 'variable_definition_name_exists_callback',
        'replace_pattern' => '[^a-z0-9_\[\]]+', // we need sqare brackets
  ),
  );
  $form['type'] = array(
      '#title' => t('Type'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => variable_definition_options_list_helper(variable_get_type()),
      '#default_value' => $item->type,
  );
  // the 'vargroup' key is a hack - see variable_der.install
  $form['vargroup'] = array(
      '#title' => t('Group'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => variable_definition_options_list_helper(variable_get_group()),
      '#default_value' => $item->vargroup,
  );
  $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $item->description,
  );
  $form['required'] = array(
      '#title' => t('Required'),
      '#type' => 'checkbox',
      '#default_value' => $item->required,
  );
  $form['token'] = array(
      '#title' => t('Token'),
      '#type' => 'checkbox',
      '#default_value' => $item->token,
  );
  $form['localize'] = array(
      '#title' => t('Localize'),
      '#type' => 'checkbox',
      '#default_value' => $item->localize,
  );
  $form['multidomain'] = array(
      '#title' => t('Multidomain'),
      '#type' => 'checkbox',
      '#default_value' => $item->multidomain,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save variable'),
      '#weight' => 40,
  );
  return $form;
}

function variable_definition_name_exists_callback($name) {
  $info = variable_get_info();
  return array_key_exists($name, $info);
}

/**
 * Form API submit callback for the variable form.
 */
function variable_definition_form_submit(&$form, &$form_state) {
  $item = entity_ui_form_submit_build_entity($form, $form_state);
  $item->save();
  module_invoke_all('variable_info_push', 'variable_definition');
  // go back to variable list
  $form_state['redirect'] = 'admin/config/system/variable/definition';
}

/**
* Form API callback for the variable group form.
*/
function variable_definition_group_form($form, &$form_state, $item, $op = 'edit') {
  if($op == 'add') {
    $item = (object)array(
      'name' => '',
      'vargroup' => 'default',
      'title' => '',
      'description' => '',
    );
  }
  $form['admin_ui_op'] = array(
      '#type' => 'hidden',
      '#value' => $op,
  );
  $form['title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $item->title,
  );
  $form['name'] = array(
      '#title' => t('Machine Name'),
      '#description' => 'Must only contain lowercase letters, numbers and underscore. Must be unique and cannot be changed later.',
      '#type' => 'machine_name',
      '#disabled' => ($op == 'edit'),
      '#default_value' => $item->name,
      '#maxlength' => 32,
      '#machine_name' => array(
  			'source' => array('title'),
  			'exists' => 'variable_definition_group_name_exists_callback'
      ),
  );
  $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $item->description,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save variable group'),
      '#weight' => 40,
  );
  return $form;
}

function variable_definition_group_name_exists_callback($name) {
  $info = variable_get_group();
  return array_key_exists($name, $info);
}

/**
 * Form API submit callback for the variable group form.
 */
function variable_definition_group_form_submit(&$form, &$form_state) {
  $var = entity_ui_form_submit_build_entity($form, $form_state);
  $var->save();
  module_invoke_all('variable_group_info_push', 'variable_definition');
  $form_state['redirect'] = 'admin/config/system/variable/groupdef';
}
