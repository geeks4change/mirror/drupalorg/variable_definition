<?php
/**
 * Variable definition UI controller
 */
class VariableDefinitionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['Title'] = 'Variable definitions';
    $items[$this->path]['description'] = 'Define variables in the UI.';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }
}

/**
 * Variable group definition UI controller
 */
class VariableGroupDefinitionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['Title'] = 'Variable group definitions';
    $items[$this->path]['description'] = 'Define variable groups in the UI.';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }
}
